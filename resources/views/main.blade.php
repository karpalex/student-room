<!DOCTYPE html>
<html>
    <head>
        <title>Weather</title>
        <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div id="root">
        </div>
        <script src="/bundle.js"></script>
    </body>
</html>
