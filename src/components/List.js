import React from 'react';
class Items extends React.Component {

    render() {
        let students = this.props.students;
        function createCities(entry, index) {
            return <li>
                <div key={index}>
                    {entry.name}
                </div>
            </li>
        }

        let listItems = students.map(createCities);

        return (
            <ul>
                {listItems}
            </ul>
        );
    }
}

export default Items