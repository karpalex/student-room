import {
    GET_WEATHER_REQUEST,
    GET_WEATHER_SUCCESS,
    ADD_CITY_REQUEST,
    ADD_CITY_SUCCESS,
    ADD_CITY_FALSE,
    DELETE_CITY,
    GET_CITIES_FROM_LOCAL
} from '../constants/Page'
import weather from '../api/weather_api'
import axios from 'axios';
import ls from 'local-storage'
function getWeather(dispatch) {
    navigator.geolocation.getCurrentPosition(function (position) {
        axios.get(weather.url_info, {
            params: {
                lat: position.coords.latitude,
                lon: position.coords.longitude,
                APPID: weather.key
            }
        }).then(function (response) {
            dispatch({
                type: GET_WEATHER_SUCCESS,
                payload: response.data
            })
        }).catch(function (error) {
                console.log(error);
        });
    });
}
function getWeatherByName(value, cities, dispatch) {
    axios.get(weather.url_info, {
        params: {
            q:value.toLowerCase(),
            APPID: weather.key
        }
    }).then(function (response) {
        cities.push(response.data);
        dispatch({
            type: ADD_CITY_SUCCESS,
            payload: cities
        });
        ls('cities', cities);
    }).catch(function (error) {
        dispatch({
            type: ADD_CITY_FALSE,
            error: new Error(error)
        })
    });

}
function addCity(value, cities) {
    return (dispatch) => {
        dispatch({
            type: ADD_CITY_REQUEST
        })
        getWeatherByName(value, cities, dispatch)

    }

}

function deleteCity(id, cities) {
    debugger;
    cities.splice(id,1);
    ls('cities', cities);
    return {
        type:DELETE_CITY,
        payload: cities
    }

}
function getUserWeather(year) {

    return (dispatch) => {
        dispatch({
            type: GET_WEATHER_REQUEST,
            payload: year
        })

        getWeather(dispatch)

    }
}
function  getCitiesFromLocal() {
    let cities  =  ls.get('cities');
    return {
        type: GET_CITIES_FROM_LOCAL,
        payload: cities ? cities: []
    }
}
module.exports = {
    getUserWeather: getUserWeather,
    addCity: addCity,
    deleteCity: deleteCity,
    getCitiesFromLocal: getCitiesFromLocal
};
