import {
    GET_WEATHER_REQUEST,
    GET_WEATHER_SUCCESS,
    GET_WEATHER_FALSE,
    ADD_CITY_REQUEST,
    ADD_CITY_SUCCESS,
    ADD_CITY_FALSE,
    DELETE_CITY,
    GET_CITIES_FROM_LOCAL
} from '../constants/Page'

const initialState = {
    students: [{name:'one'}, {name:"two"}],
    fetching: false,
    error: '',
}

export default function page(state = initialState, action) {

    switch (action.type) {
        case GET_WEATHER_REQUEST:
            return {...state, fetching: true}
        case ADD_CITY_REQUEST:
            return {...state, fetching: true};
        case ADD_CITY_SUCCESS:
            return {...state, cities:action.payload, fetching: false}
        case GET_WEATHER_SUCCESS:
            return {...state, city: action.payload, fetching: false};
        case ADD_CITY_FALSE:
            return {...state, fetching: false, error: true}
        case GET_WEATHER_FALSE:
            return {...state, fetching: false, error: true};
        case DELETE_CITY:
            return {...state, cities:action.payload, fetching: false}
        case GET_CITIES_FROM_LOCAL:
            return {...state, cities:action.payload}
        default:
            return state;
    }

}
