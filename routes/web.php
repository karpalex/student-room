<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

Route::get('/', function () {
    return view('main');
});

Route::get('/chat', 'ChatController@getChatView');
Route::post('/chat', 'ChatController@chat');

Route::get('/home', 'HomeController@index');
Route::get('/signup', function () {

});


Route::get('/restricted', [

]);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
