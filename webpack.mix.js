let mix = require('laravel-mix');

var path = require('path')
var webpack = require('webpack')
var autoprefixer = require('autoprefixer');
var precss = require('precss');

mix.webpackConfig({
    resolve: {
        modules: [
            path.join(__dirname, "src"),
            "node_modules"
        ]
    },
    entry: [
        './src/index'
    ],
    devtool: 'eval',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/public/'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: [
                    'react-hot-loader',
                    'babel-loader'
                ],
                include: path.join(__dirname, 'src')
            },
            {
                test:   /\.css$/,
                loader: "style-loader!css-loader!postcss-loader"
            }
        ],

    },
});
